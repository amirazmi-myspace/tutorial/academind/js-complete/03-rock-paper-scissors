const startGameBtn = document.getElementById('start-game-btn');

const ROCK = 'ROCK';
const PAPER = 'PAPER';
const SCISSORS = 'SCISSORS';
const DEFAULT = ROCK;
const DRAW = 'DRAW';
const P_WINS = 'PLAYER WON';
const C_WINS = 'COMPUTER WON';
let gameIsRunning = false; 

const getPlayerChoice = () => {
    const selection = prompt(`${ROCK}, ${PAPER} or ${SCISSORS}`, '').toUpperCase();
    if (selection !== ROCK && selection !== PAPER && selection !== SCISSORS) {
        alert(`Invalid Choice!! We chose ${DEFAULT} for you!`);
        return;
    }
    return selection;
};

const getComputerChoice = () => {
    const randomValue = Math.random()
    if (randomValue < 0.34) {
        return ROCK;
    } else if (randomValue < 0.67) {
        return PAPER;
    } else {
        return SCISSORS;
    }
};

const getWinner = (choiceComp, choicePlayer = DEFAULT) => choiceComp === choicePlayer ? DRAW : choiceComp === ROCK && choicePlayer === PAPER || choiceComp === PAPER && choicePlayer === SCISSORS || choiceComp === SCISSORS && choicePlayer === ROCK ? P_WINS : C_WINS;

    // if (choiceComp === choicePlayer) {
    //     return DRAW;
    // } else if (choiceComp === ROCK && choicePlayer === PAPER || choiceComp === PAPER && choicePlayer === SCISSORS || choiceComp === SCISSORS && choicePlayer === ROCK) {
    //     return P_WINS;
    // } else {
    //     return C_WINS;
    // }


startGameBtn.addEventListener('click', () => {
    if (gameIsRunning) {
        return;
    }
    gameIsRunning = true;
    console.log('The game is starting');
    const playerSelection = getPlayerChoice();
    const computerChoice = getComputerChoice();
    let winner;
    if (playerSelection) {
        winner = getWinner(computerChoice, playerSelection);
    } else {
        winner = getWinner(computerChoice, playerSelection)
    }

    let message = `You picked ${playerSelection || DEFAULT}, computer picked ${computerChoice}, therefore you `;

    if (winner === DRAW) {
        message += 'had a draw.';
    } else if (winner === P_WINS) {
        message += 'won.';
    } else {
        message += 'lost.';
    }
    alert(message);
    gameIsRunning = false;
});

// not related
const sumUp = (...numbers) => {
    const validateNumber = (number) => {
        return isNaN(number) ? 0 : number;
    };
    let sum = 0;
    for (const num of numbers) {
        sum += validateNumber(num);
    }
    return sum;
};

console.log(sumUp(1,23,4,56,34))